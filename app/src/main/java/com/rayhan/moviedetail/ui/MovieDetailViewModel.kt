package com.rayhan.moviedetail.ui

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.rayhan.moviedetail.api.ApiConfig
import com.rayhan.moviedetail.api.model.ResponseDetails
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MovieDetailViewModel(application: Application) : AndroidViewModel(application) {
    private val client = ApiConfig.getApiService(application)
    private val tag = "MovieDetailViewModel"

    private val _movieDetail = MutableLiveData<ResponseDetails>()
    val movieDetail: LiveData<ResponseDetails> = _movieDetail

    fun getMovieDetails() {
        client.getDetails().enqueue(object : Callback<ResponseDetails> {
            override fun onResponse(
                call: Call<ResponseDetails>,
                response: Response<ResponseDetails>
            ) {
                if (response.isSuccessful) {
                    _movieDetail.value = response.body()

                    if (_movieDetail.value == null)
                        Log.i(tag, "Response detail berisi null")
                }
            }

            override fun onFailure(call: Call<ResponseDetails>, t: Throwable) {
                Log.e(tag, "Response detail gagal didapat")
            }
        })
    }

}