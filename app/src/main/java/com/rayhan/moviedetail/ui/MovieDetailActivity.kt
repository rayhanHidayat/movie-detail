package com.rayhan.moviedetail.ui

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.rayhan.moviedetail.databinding.ActivityMovieDetailBinding
import jp.wasabeef.glide.transformations.RoundedCornersTransformation

class MovieDetailActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMovieDetailBinding
    private val movieDetailViewModel by viewModels<MovieDetailViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMovieDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)

        movieDetailViewModel.getMovieDetails()

        movieDetailViewModel.movieDetail.observe(this, { data ->
            binding.run {
                val poster = data.posterPath
                val backdrop = data.backdropPath

                tvTitle.text = data.title
                tvBahasaIsi.text = data.originalLanguage
                tvDateIsi.text = data.releaseDate
                tvNumAvg.text = data.voteAverage.toString()
                tvNumCount.text = data.voteCount.toString()
                tvOverview.text = data.overview
                tvTagline.text = data.tagline

                Glide.with(this@MovieDetailActivity)
                    .load("https://image.tmdb.org/t/p/w500$poster")
                    .apply(
                        RequestOptions.bitmapTransform(RoundedCornersTransformation(8, 0))
                    )
                    .into(imgPoster)
                Glide.with(this@MovieDetailActivity)
                    .load("https://image.tmdb.org/t/p/original$backdrop")
                    .apply(
                        RequestOptions.bitmapTransform(RoundedCornersTransformation(8, 0))
                    )
                    .into(imgBackdrop)
            }

        })
    }
}