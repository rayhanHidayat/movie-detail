package com.rayhan.moviedetail.api

import android.content.Context
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class ApiConfig {
    companion object {

        private const val API_KEY = "INSERT_API_KEY_HERE"
        private const val BASE_URL = "https://api.themoviedb.org/3/"

        fun getApiService(context: Context): ApiService {
            val client = OkHttpClient.Builder().addInterceptor { chain ->
                val url = chain.request().url().newBuilder()
                    .addQueryParameter("api_key", API_KEY)
                    .build()
                val newRequest = chain.request().newBuilder().url(url).build()
                chain.proceed(newRequest)
            }.build()
            val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build()
            return retrofit.create(ApiService::class.java)
        }
    }
}