package com.rayhan.moviedetail.api

import com.rayhan.moviedetail.api.model.ResponseDetails
import retrofit2.Call
import retrofit2.http.GET

interface ApiService {

    @GET("movie/550")
    fun getDetails(): Call<ResponseDetails>
}